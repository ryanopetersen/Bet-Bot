#----------------------------------------------------------------------
# ARBot.py
# Version 0.2
# Changelog:
#   v0.3:
#       * Now replies to users if they forgot a command
#       * Only opens thread if have a score higher than 15
#   v0.2:
#       * Switched to sqlite database model
#       * Only controlled by one class rather than three
#----------------------------------------------------------------------

import praw
import re
import time
import datetime
import dataset
import sqlalchemy
import urllib2
from difflib import SequenceMatcher
from message_templates import *

#-------------------------------------------------------------------------------

class BetBot:

    ''' Class for functions of the BetBot
        as seen on reddit/r/betonaskreddit
    '''

    def __init__(self,
                 db,
                 r,
                 bet_sub,
                 pull_sub,
                 username,
                 password,
                 user_agent,
                 debug=False):

        '''
            pre: db-database, r-praw reddit object
            post: database, reddit object, as well as subbreddit vars are
                  initialzed.
        '''
        self.db = db
        self.users = self._createTable('users')
        self.threads = self._createTable('threads')
        self.replied_to = self._createTable('replied_to')
        self.no_command_replied = self._createTable('no_command_replied')
        self.r = r
        self.username = username
        self.password = password
        self.bet_sub = self.r.get_subreddit(bet_sub)
        self.pull_sub = self.r.get_subreddit(pull_sub)
        self.debug=debug

        users_table_columns = {'username': sqlalchemy.String,
                                    'total_money': sqlalchemy.Float}
        self._createColumns(self.users, users_table_columns)

        threads_table_columns = {'thread_title': sqlalchemy.String,
                                      'bet_thread_id': sqlalchemy.String,
                                      'pull_thread_id': sqlalchemy.String,
                                      'time_opened': sqlalchemy.Float,
                                      'time_closed': sqlalchemy.Float,
                                      'is_thread_open': sqlalchemy.Boolean,
                                      'date_posted': sqlalchemy.DateTime}
        self._createColumns(self.threads, threads_table_columns)

        replied_to_table_columns = {'comment_id': sqlalchemy.String,
                                    'date_replied': sqlalchemy.DateTime}
        self._createColumns(self.replied_to, replied_to_table_columns)

        no_command_table_columns = {'comment_id': sqlalchemy.String,
                                    'date_replied': sqlalchemy.DateTime}
        self._createColumns(self.no_command_replied, no_command_table_columns)

        self.bet_table_columns = {'user': sqlalchemy.BigInteger,
                                  'bet_amount': sqlalchemy.Float,
                                  'bet_string': sqlalchemy.String,
                                  'time_of_bet': sqlalchemy.Float,
                                  'bet_thread_id': sqlalchemy.String}

#-------------------------------------------------------------------------------

    def _createTable(self, tablename):

        '''
            pre: table name to create
            post: creates or loads table
        '''

        return self.db.get_table(tablename)


#-------------------------------------------------------------------------------

    def _createColumns(self, table, columns):

        '''
            Creates columns for the tables:
            pre: a table object and a columns dictionary
                 dictionary must be in format of:
                 \{column_name: sqlalchemy.datatype\}
            post: columns in table are created.
        '''

        for c_name, c_type in columns.iteritems():
            table.create_column(c_name, c_type)

#-------------------------------------------------------------------------------

    def start(self, handle_bets = True):
        '''
            pre: none
            post: starts the BetBot
        '''

        print "Logging in..."
        self._login()
        print "Starting bet crawling..."
        if handle_bets == True:
            self._bettingLoop()
        else:
            self._checkForBetBotComments()
#-------------------------------------------------------------------------------

    def _login(self):
        '''
            pre: none
            post: logs in to reddit
        '''

        self.r.login(self.username, self.password)

#-------------------------------------------------------------------------------

    def _bettingLoop(self):

        '''
            pre: none
            post: runs the main loop of the bot
        '''

        t = time.time()

        while True:
            try:
                print "Fetching New Users..."
                self._fetchNewUsers()

                # Only open threads once every 2 hours
                if (t + (3600 * 2)) < time.time():
                    print "Opening new threads..."
                    self._openThreads()
                    t = time.time()

                print "Gathering bets..."
                self._gatherBets()

                print "Closing threads..."
                self._closeThreads()

                print "Gathering results..."
                self._getResults()

            except urllib2.HTTPError, e:
                if e.code in [429, 500, 502, 503, 504]:
                    print "Reddit is down (Error: {}), sleeping...".format(e.code)
                    time.sleep(60)
                    pass
                else:
                    raise
            except Exception, e:
                print "couldn't Reddit: {}".format(str(e))
                raise
    #-------------------------------------------------------------------------------

    def _fetchNewUsers(self):

        '''
            pre: none
            post: table users in database is modifed to any new users if
                  found.
        '''
        new_users_sticky = self.r.get_submission(submission_id='37mz6a')
        flat_comments = praw.helpers.flatten_tree(new_users_sticky.comments)
        for comment in flat_comments:
            if self.users.find_one(username = str(comment.author)) == None:
                new_user = self._parseObject('comment', comment)
                self.users.insert(dict(username=new_user[0],
                                       total_money=1000))
                self._post('registered', comment)

#-------------------------------------------------------------------------------

    def _openThreads(self):

        '''
            pre: none
            post: new threads are created pulled from pull subreddit and added
                  to the table threads a post is made for them by calling
                  _post()
        '''
        max_open = 0
        for submission in self.pull_sub.get_rising(limit = 1000):
            if submission.score > 15 and max_open < 6:
                # Ignore serious threads, because those aren't circlejerky enough
                if not re.search("serious", str(submission), re.IGNORECASE):
                    # Check to make sure it hasn't already been posted
                    if self.threads.find_one(pull_thread_id = str(submission.id)) == None:
                        pull_thread_info = self._parseObject('submission', submission)
                        open_bet_post = self._post('open', pull_thread_info)
                        self.threads.insert(dict(thread_title = open_bet_post[0],
                                                 bet_thread_id = open_bet_post[1],
                                                 pull_thread_id = pull_thread_info[1],
                                                 time_opened = float(time.time()),
                                                 is_thread_open = True,
                                                 date_posted = datetime.datetime.utcnow()))
                        subm = self.r.get_submission(submission_id = open_bet_post[1])
                        subm.set_flair(flair_css_class = 'open',
                                       flair_text = 'Betting Open')
                        max_open += 1


#-------------------------------------------------------------------------------

    def _closeThreads(self):

        '''
            pre: none
            post: if a thread is open for more than 6 hours the threads table
                  is modifed to set it as closed, and adds a time closed stamp.
        '''

        open_threads = self.threads.find(is_thread_open = True)
        for thread in open_threads:
            if (thread['time_opened'] + float(21600)) < time.time():
                thread_id = thread['id']
                self.threads.update(dict(id = thread_id,
                                         is_thread_open = False,
                                         time_closed = time.time()),
                                         ['id'])

                closed_thread = self.r.get_submission(submission_id = thread['bet_thread_id'])
                closed_thread.set_flair(flair_css_class = 'closed',
                                        flair_text = 'Betting Closed')

#-------------------------------------------------------------------------------

    def _gatherBets(self):

        '''
            pre: none
            post: all bets from open threads are gathered and sent to _makeBet
        '''

        open_threads = self.threads.find(is_thread_open = True)
        for thread in open_threads:
            # get the praw submission object
            submission = self.r.get_submission(submission_id = thread['bet_thread_id'])
            flat_comments = praw.helpers.flatten_tree(submission.comments)
            for comment in flat_comments:
                # Check if bot has already replied to user
                if not self._checkIfBotAlreadyReplied(comment):
                    # Check if bot was summoned
                    if re.search("BetBot! Bet", str(comment), re.IGNORECASE):
                        print "Found one!"
                        comment_info = self._parseObject('comment', comment)
                        # If user is not registered, tell them to get registered
                        if not self._checkIfRegistered(comment_info[0]):
                            comment_info.append(comment)
                            self._post('get_registered', comment_info[0])
                        # Make the bet
                        else:
                            bet_list = str(comment).split(" ")
                            self._makeBet(comment, comment_info, bet_list[2:], time.time(), thread)

#-------------------------------------------------------------------------------

    def _makeBet(self, comment, comment_info, bet_list, time, thread):

        '''
            pre: username, a list of bet information, and database row
                 of corresponding thread.
            post: a table of all bets made on thread is created
        '''
        # test to make sure bet amount can be convereted
        try:
            float(bet_list[0])
        except ValueError:
            print "Value Error on Bet post... letting user know"
            self._post('error', comment)
            return False


        bet_amount = float(bet_list[0])
        bet_string = " ".join(bet_list[1:])
        username = comment_info[0]
        # append the comment object so the bot can reply to it
        comment_info.append(comment)
        # Loads table if it currently exists, or creates it if it does not
        # Table is named after the betting thread id
        bet_table = self.db.get_table(thread['bet_thread_id'])
        self._createColumns(bet_table, self.bet_table_columns)

        user = self.users.find_one(username=username)
        if user['total_money'] < bet_amount:
            self._post('too_much', comment_info)
        elif bet_table.find_one(user = user['username']) != None:
            self._post('already_bet', comment_info)
        else:
            bet_table.insert(dict(user = user['username'],
                                  bet_amount = round(bet_amount, 2),
                                  bet_string = bet_string,
                                  time_of_bet = float(time),
                                  bet_thread_id = thread['bet_thread_id']))
            new_total = user['total_money'] - bet_amount
            data = dict(id = user['id'], total_money = new_total)
            self.users.update(data, ['id'])
            self._post('bet_made', comment_info)

#-------------------------------------------------------------------------------

    def _getResults(self):

        '''
            pre: none
            post: pulls all closed threads and calculates the winners from
                  each thread, calls _post to make a results thread,
                  updates users table with new values
                  drops betting table for thread
        '''

        closed_threads = self.threads.find(is_thread_open = False)
        for thread in closed_threads:
            if (thread['time_closed'] + float(21600)) < time.time():
                winner_table = self._findWinners(thread['bet_thread_id'], thread['pull_thread_id'])
                winner_table = self._calcuateWinnings(winner_table)

                # biggest_win = self.db.query('SELECT MAX(total_money) FROM winners')
                # for row in biggest_win:
                #     big_win_total = row['MAX(total_money)']

                winner_strings = self._createWinnerStrings(winner_table)

                winner_info = [winner_strings, thread['pull_thread_id']]
                results_thread = self._post('results', winner_info)
                results_thread.set_flair(flair_css_class = 'results',
                                        flair_text = 'Results')
                self._messageWinners(winner_table, results_thread)
                self.threads.delete(id=thread['id'])

                self._cleanUp(winner_table, thread['bet_thread_id'])
#-------------------------------------------------------------------------------

    def _findWinners(self, thread_table_name, pull_thread_id):

        '''
            pre: thread table name to calculate winners
            post: returns a dictionary of winners
        '''
        thread_table = self.db.get_table(thread_table_name)
        pull_thread = self.r.get_submission(submission_id = pull_thread_id,
                                            comment_limit = 5,
                                            comment_sort = 'top')
        top_five_comments = pull_thread.comments[0:5]

        winner_table = self._createTable('winners')
        winner_columns = {'username': sqlalchemy.String,
                           'bet_amount': sqlalchemy.Float,
                           'bet_string': sqlalchemy.String,
                           'thread_id': sqlalchemy.String,
                           'comment_time': sqlalchemy.Float,
                           'place':  sqlalchemy.Integer,
                           'amount_won': sqlalchemy.Float}

        self._createColumns(winner_table , winner_columns)

        # check users bets
        # if the users bet string is contained in the comment and that comment exists
        # add them to the winner table
        for user in thread_table:
            for top_comment in top_five_comments:
                if self._similarity(user['bet_string'], str(top_comment)) >= .3:
                    if winner_table.find_one(username = user['user']) == None:
                        winner_table.insert(dict(username = user['user'],
                                                 bet_amount = round(user['bet_amount'], 2),
                                                 bet_string = user['bet_string'],
                                                 thread_id = thread_table_name,
                                                 comment_time = user['time_of_bet'],
                                                 place = (top_five_comments.index(top_comment) + 1)))
                    break

        return winner_table

#-------------------------------------------------------------------------------

    def _similarity(self, user_bet, top_comment):
        '''
            pre: a string of the user bet, and the top comment
            post: returns the ratio of how similar the strings are
        '''

        return SequenceMatcher(None, user_bet, top_comment).ratio()

#-------------------------------------------------------------------------------

    def _calcuateWinnings(self, winner_table):

        '''
            pre: winners table
            post: winnings for each winner are added to their account

        '''
        for user in winner_table:
            # current time subtracted by comment time, divided by 12 hours
            time_divisor = abs((time.time() - user['comment_time']) / 43200)
            amount_won_on_bet = (user['bet_amount'] * (6 - user['place'])) / time_divisor
            # update the amount won on the winner table
            data = dict(id = user['id'], amount_won = round(amount_won_on_bet, 2))
            winner_table.update(data,['id'])
            # update the total money on user table
            user_total = self.users.find_one(username = user['username'])
            new_total = user_total['total_money'] + amount_won_on_bet
            new_data = dict(id = user_total['id'], total_money = round(new_total, 2))
            self.users.update(new_data, ['id'])
        return winner_table

#-------------------------------------------------------------------------------

    def _createWinnerStrings(self, winner_table):

        '''
            pre: winners table
            post: list of winners strings
        '''
        winner_strings = []

        first_place = winner_table.find(place = 1)
        first_place_string = ""
        for user in first_place:
            first_place_string += ('\t\t* {} won {} with {}\n'.format(user['username'],
                                                                      user['amount_won'],
                                                                      user['bet_string']))
        winner_strings.append(first_place_string)

        second_place = winner_table.find(place = 2)
        second_place_string = ""
        for user in second_place:
            second_place_string += ('\t\t* {} won {} with {}\n'.format(user['username'],
                                                                      user['amount_won'],
                                                                      user['bet_string']))
        winner_strings.append(second_place_string)

        third_place = winner_table.find(place = 3)
        third_place_string = ""
        for user in third_place:
            third_place_string += ('\t\t* {} won {} with {}\n'.format(user['username'],
                                                                      user['amount_won'],
                                                                      user['bet_string']))
        winner_strings.append(third_place_string)

        fourth_place = winner_table.find(place = 4)
        fourth_place_string = ""
        for user in fourth_place:
            fourth_place_string += ('\t\t* {} won {} with {}\n'.format(user['username'],
                                                                      user['amount_won'],
                                                                      user['bet_string']))
        winner_strings.append(fourth_place_string)

        fifth_place = winner_table.find(place = 5)
        fifth_place_string = ""
        for user in fifth_place:
            fifth_place_string += ('\t\t* {} won {} with {}\n'.format(user['username'],
                                                                      user['amount_won'],
                                                                      user['bet_string']))
        winner_strings.append(fifth_place_string)

        return winner_strings
#-------------------------------------------------------------------------------

    def _messageWinners(self, winner_table, results_thread):

        '''
            pre: winners dictionary, and the results thread
            post: class _sendMessage() to send the winners a message
        '''
        for user in winner_table:
            self._sendMessage('winner', [user['username'], user['amount_won'],
                              str(results_thread.title), str(results_thread.id)])

#-------------------------------------------------------------------------------

    def _cleanUp(self, winner_table, thread_table_name):

        '''
            pre: a winner table, and a bet thread table name
            post: both tables are dropped
        '''
        print "Cleaning up the tables..."
        winner_table.drop()
        self.db[thread_table_name].drop()

#-------------------------------------------------------------------------------

    def _checkForBetBotComments(self):

        '''
            pre: none
            post: scrapes for Bank comments and replies to them
        '''
        while True:
            open_threads = self.threads.find(is_thread_open = True)
            print "Checking open threads..."
            for thread in open_threads:
                submission = self.r.get_submission(submission_id = thread['bet_thread_id'])
                flat_comments = praw.helpers.flatten_tree(submission.comments)
                for comment in flat_comments:
                    if re.search("BetBot!", str(comment), re.IGNORECASE):
                        # Check if bank comment
                        if re.search("BetBot! Bank", str(comment), re.IGNORECASE):
                            if not self._checkIfBotAlreadyReplied(comment):
                                self._post('bank', comment)
                        elif re.search("BetBot! Bet", str(comment), re.IGNORECASE):
                            pass
                        else:
                            if self.no_command_replied.find_one(comment_id = str(comment.id)) == None:
                                self._post('no_command', comment)

            # Check results threads
            results_threads = self.r.search('flair:"results"', subreddit='betonaskreddit', limit = 20)
            print "Checking results threads..."
            for thread in results_threads:
                 flat_comments = praw.helpers.flatten_tree(thread.comments)
                 for comment in flat_comments:
                     if re.search("BetBot!", str(comment), re.IGNORECASE):
                         # Check if bank comment
                         if re.search("BetBot! Bank", str(comment), re.IGNORECASE):
                             if not self._checkIfBotAlreadyReplied(comment):
                                 self._post('bank', comment)
                         elif re.search("BetBot! Bet", str(comment), re.IGNORECASE):
                             pass
                         else:
                             if self.no_command_replied.find_one(comment_id = str(comment.id)) == None:
                                 self._post('no_command', comment)
#-------------------------------------------------------------------------------

    def _checkIfBotAlreadyReplied(self, comment):

        '''
            pre: a comment object
            post: returns a boolean, true if bot as already replied, False
                  if not.
        '''

        if self.replied_to.find_one(comment_id = str(comment.id)) != None:
            return True
        else:
            return False


#-------------------------------------------------------------------------------

    def _checkIfRegistered(self, username):

        '''
            pre: username for the person to check
            post: returns True if they are registered, False if not.
        '''

        if self.users.find_one(username = username) == None:
            return False
        else:
            return True

#-------------------------------------------------------------------------------

    def _post(self, type_of_post, info):

        '''
            pre: type-string, info-list or dic depending of type
            post: a thread is posted of the corresponding type in the
                  bet sub. Returns parsed submission object.
        '''

        #-----------------------------------------------------------------------

        def openBet(info):
            bet_title = OPEN_POST_TITLE.format(info[0])
            bet_body = OPEN_THREAD_BODY.format(info[0], info[1])

            print "Bot posting new open thread..."
            if self.debug == True:
                print bet_title + '\n' + bet_body
                return info
            else:
                bet_post = self.r.submit(self.bet_sub, bet_title, text = bet_body)
                return self._parseObject('submission', bet_post)

        #-----------------------------------------------------------------------

        def results(info):
            submission_title = str(self.r.get_submission(submission_id = info[1]).title)
            winners_strings = info[0]
            results_title = RESULTS_POST_TITLE.format(submission_title)
            results_body = RESULTS_THREAD_BODY.format(submission_title,
                                                      winners_strings[0],
                                                      winners_strings[1],
                                                      winners_strings[2],
                                                      winners_strings[3],
                                                      winners_strings[4])

            print "Bot posting new results thread..."
            if self.debug == True:
                print results_title + '\n' + results_body
                return info
            else:
                results_post = self.r.submit(self.bet_sub, results_title, text = results_body)
                return results_post

        #-----------------------------------------------------------------------

        def registered(info):
            msg = REGISTERED_REPLY

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                info.reply(msg)
                self.replied_to.insert(dict(comment_id = str(info.id),
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def get_registered(info):
            comment = info[-1]
            msg = GET_REGISTERED_REPLY

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                comment.reply(msg)
                self.replied_to.insert(dict(comment_id = info[1],
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def too_much(info):
            comment = info[-1]
            user= self.users.find_one(username=info[0])
            user_money = user['total_money']
            msg = TOO_MUCH_REPLY.format(user_money)

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                comment.reply(msg)
                self.replied_to.insert(dict(comment_id = info[1],
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def bet_made(info):
            comment = info[-1]
            msg = BET_MADE_REPLY

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                comment.reply(msg)
                self.replied_to.insert(dict(comment_id = info[1],
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def already_bet(info):
            comment = info[-1]
            msg = BET_TWICE_REPLY

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                comment.reply(msg)
                self.replied_to.insert(dict(comment_id = info[1],
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def bank(info):
            user = self.users.find_one(username = str(info.author))
            msg = BANK_REPLY.format(user['total_money'])

            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                info.reply(msg)
                self.replied_to.insert(dict(comment_id = info.id,
                                            date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def error(info):
            if self.no_command_replied.find_one(comment_id = str(comment.id)) == None:
                if self.debug == True:
                    pass
                else:
                    print "Replying to user..."
                    info.reply("Sorry, but you didn't put a number there, edit it to add one!")
                    self.no_command_replied.insert(dict(comment_id = info.id,
                                                        date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        def no_command(info):
            msg = FORGOT_COMMAND_REPLY
            if self.debug == True:
                print msg
            else:
                print "Replying to user..."
                info.reply(msg)
                self.no_command_replied.insert(dict(comment_id = info.id,
                                                    date_replied = datetime.datetime.utcnow()))

        #-----------------------------------------------------------------------

        options = { 'open': openBet,
                    'results': results,
                    'registered': registered,
                    'get_registered': get_registered,
                    'too_much': too_much,
                    'bet_made': bet_made,
                    'already_bet': already_bet,
                    'bank': bank,
                    'error': error,
                    'no_command': no_command
        }

        return options[type_of_post](info)

#-------------------------------------------------------------------------------

    def _parseObject(self, obj_type, obj):

        '''
            pre: obj - a praw reddit obj to be converted
            post: returns a list of the relevant information converted to
                  python types.
        '''

        #-----------------------------------------------------------------------

        def comment(obj):
            return [str(obj.author), str(obj.id)]

        #-----------------------------------------------------------------------

        def submission(obj):
            return [str(obj.title), str(obj.id)]

        #-----------------------------------------------------------------------

        def user(obj):
            pass

        #-----------------------------------------------------------------------

        def subreddit(obj):
            pass

        #-----------------------------------------------------------------------

        options = { 'comment': comment,
                    'submission': submission,
                    'user': user,
                    'subreddit': subreddit
        }

        return options[obj_type](obj)
#-------------------------------------------------------------------------------

    def _sendMessage(self, group, info):

        '''
            pre: type-type of message to send, users-list of users to message
            post: PMs are sent to each users
        '''

        #-----------------------------------------------------------------------

        def winner(info):
            sub = "Congrats you won!"
            msg = WINNER_MESSAGE.format(info[2],
                                        info[1],
                                        info[3])
            if self.debug == True:
                print msg
            else:
                self.r.send_message(info[0], sub, msg, captcha=None)

        #-----------------------------------------------------------------------

        options = { 'winner': winner }

        options[group](info)

#-------------------------------------------------------------------------------
